-- Account staging table. Records will be deleted from here once created in Salesforce
create table account (
    id bigserial primary key, -- customer account no. Salesforce external ID
    name varchar(50), -- account name
    is_guest boolean, -- indicates guest ('pseudo') account
    created_ts timestamp with time zone not null default current_timestamp,
    modified_ts timestamp with time zone not null default current_timestamp
);

-- trigger to update modified_ts
create trigger trg_account_modified_ts before update on account for each row execute procedure fn_update_modified_ts();

