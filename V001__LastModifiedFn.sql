-- utility function to update last modified timestamp
create or replace function fn_update_modified_ts()
returns trigger 
as 
$$
begin
    -- ASSUMES the table has a column named exactly "modified_ts".
    -- Fetch date-time of actual current moment from clock, rather than start of statement or start of transaction.
    new.modified_ts = current_timestamp; 
    return new;
end;
$$ 
language 'plpgsql';
