-- Contact staging table. Records will be deleted from here once created in Salesforce
create table contact (
    id bigint primary key, -- contact id.
    account_id bigint not null references account(id), -- FK to account
    contact_type varchar(10), -- GUEST or REGISTERED
    first_name varchar(100),
    last_name varchar(100),
    email varchar(100),
    date_of_birth date,
    opt_in_fishefax boolean,
    opt_in_dedjtr boolean,
    opt_in_vrfish boolean,
    created_ts timestamp with time zone not null default current_timestamp,
    modified_ts timestamp with time zone not null default current_timestamp
);

-- trigger to update modified_ts
create trigger trg_contact_modified_ts before update on contact for each row execute procedure fn_update_modified_ts();
