-- Address staging table. Records will be deleted from here once created in Salesforce
create table address (
    id bigserial primary key, -- address PK.
    contact_id bigint not null references contact(id), -- FK to contact
    -- TODO align address either 100% with Salesforce or 100% with AS4590 standard
    street_number varchar(100),
    street_name_1 varchar(100),
    street_name_2 varchar(100),
    city varchar(100),
    state varchar(100),
    postcode varchar(20),
    country varchar(100),
    created_ts timestamp with time zone not null default current_timestamp,
    modified_ts timestamp with time zone not null default current_timestamp
);

-- trigger to update modified_ts
create trigger trg_address_modified_ts before update on address for each row execute procedure fn_update_modified_ts();

